﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using System.Security.Cryptography;
using System.Text;

namespace CodedLobby
{
    [System.Serializable]
    public class Match
    {
        public string matchID;
        public SyncListGameObject players = new SyncListGameObject();

        public Match(string matchID, GameObject player)
        {
            this.matchID = matchID;
            players.Add(player);
        }

        public Match ()
        {

        }
    }
    [System.Serializable]
    public class SyncListGameObject : SyncList<GameObject>
    {

    }

    public class SyncListMatch : SyncList<Match>
    {

    }

    public class MatchMaker : NetworkBehaviour
    {
        public static MatchMaker instance;

        public SyncListMatch matches = new SyncListMatch();

        public SyncList<String> matchIDs = new SyncList<String>();  //Guide showed SyncListString instead of SyncList<string>
        void start()
        {
            instance = this;
        }
        public bool HostGame(string _matchID, GameObject _player)
        {
            if (!matchIDs.Contains(_matchID))
            {   //Validation if Match ID is unique
                matchIDs.Add(_matchID);
                matches.Add(new Match(_matchID, _player));
                Debug.Log($"Match ID Generated");
                return true;
            }
            else
            {
                Debug.Log($"Match ID already exists");
                return false;
            }
        }

        public static string GetRandomMatchID ()
        {
            string _id = string.Empty;
            for (int i = 0; i < 5; i++)
            {
                int random = UnityEngine.Random.Range(0, 26);
                _id += (char)(random + 65);
            }
            Debug.Log($"Random Match ID: {_id}");
            return _id;
        }
 
    }
    
    public static class MatchExtensions
    {
        public static Guid ToGuid (this string id) 
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            byte[] inputBytes = Encoding.Default.GetBytes(id);
            byte[] hashBytes = provider.ComputeHash(inputBytes);

            return new Guid(hashBytes);
        }
    }
}