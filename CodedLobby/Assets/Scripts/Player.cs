﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace CodedLobby
{

    public class Player : NetworkBehaviour
    {
        
        public static Player localPlayer;
        [SyncVar] public string matchID;

        NetworkMatchChecker networkMatchChecker;
        
        void start()
        {
            if (localPlayer)
            {
                localPlayer = this;
                
            }
            networkMatchChecker = GetComponent<NetworkMatchChecker>();
        }

        public void HostGame()
        {
            string matchID = MatchMaker.GetRandomMatchID();
            CmdHostGame(matchID);

        }

        [Command]
        void CmdHostGame(string _matchID)
        {
            matchID = _matchID;
            if (MatchMaker.instance.HostGame(_matchID, gameObject))
            {
                Debug.Log($"<color = green>Game Hosted Successfully</color>");
                networkMatchChecker.matchId = _matchID.ToGuid();
                TargetHostGame(true, _matchID);
            }
            else
            {
                Debug.Log($"<color = red>Game Hosted Failed</color>");
                TargetHostGame(false, _matchID);
            }
        }
        
        [TargetRpc]
        void TargetHostGame (bool success, string _matchID)
        {
            Debug.Log($"Match ID: {matchID} == {_matchID}");
            UILobby.instance.HostSuccess(success);
        }

    }
}