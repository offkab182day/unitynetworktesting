﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace CodedLobby
{
    public class AutoHostClient : MonoBehaviour
    {
        [SerializeField] NetworkManager networkManager;


        void start()
        {
            if (!Application.isBatchMode) //batch means headless build
            {
                Debug.Log($"=== client build ===");
                networkManager.StartClient();
            }
            else
            {
                Debug.Log($"=== server build ===");
            }

        }

        public void JoinLocal()
        {
            networkManager.networkAddress = "localhost";
            networkManager.StartClient();
        }

    }
}

